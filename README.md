[[_TOC_]]
## Steps to configure Spring Cloud Config

### Project dependencies
```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
```
- Spring Cloud dependency

```xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```
### Add the annotation in the Entry file
```java
@SpringBootApplication
@EnableConfigServer
public class ConfigServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }
}
```

### Configure the `bootstrap.yaml` file 
```yaml
spring:
  application:
    name: configserver
```
### Configure the `application.yaml` file
- Refer to **1.0.0-file-location-config** tag
```yml
spring:
  profiles:
    active: native
  cloud:
    config:
      server:
        native:
          search-locations: file:///E:/config/licensingservice,file:///E:/config/assetmgmt,file:///E:/config/organizationservice
server:
  port: 8888
```
### In case of Git repo use the below configuration 
- Refer to **2.0.0-private-git-repo** tag

```yaml
spring:
  cloud:
    config:
      server:
        git:
          uri:
          repos:
            assetmgmt:
              uri: git@gitlab.com:assetmgmt-microservices/spring-cloud-assetmgmt-config.git
              clone-on-start: true
            licensingservice:
              uri: git@gitlab.com:assetmgmt-microservices/spring-cloud-orgservice-config.git
              clone-on-start: true
server:
  port: 8888
```

### Encrypting the sensitive information in `bootstrap.yaml` file
```yaml
encrypt:
  key: TESTING
```
### Disabling the decryption on the server side `bootstrap.yaml` 
```yaml
spring: 
  cloud:
      config:
        server:
          encrypt:
            enabled: false
```

